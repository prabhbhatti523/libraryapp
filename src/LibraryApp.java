import java.util.ArrayList;
import java.util.List;

public class LibraryApp {
	
	private boolean isLoggedIn = false;
	
	private List<Book> books = new ArrayList<Book>();
	public LibraryApp() {
		
		// @TODO:  Admin logged in = false;
		// @TODO:  Number of books = 0
		
	}
	
	public boolean adminLoggedIn() {
		return this.isLoggedIn;
	}
	public boolean adminLogin(String password) {
		
		if(password.equals("adminadmin")) {
			isLoggedIn = true;
			
		}
		
		
		else this.isLoggedIn = false;
		
		return this.isLoggedIn;
	}

	//@TODO: Implement code for this
		// Reminder - there needs to be a isEmpty() function somehwere
	public List<Book> getBooks() {
 		// this should probably return something
		return this.books;
 	}
		
		public void addBook(Book book) throws OperationNotAllowedException{
					 // check if admin is logged in BEFORE adding a book
					 if (this.isLoggedIn == true) {
						 this.books.add(book);	 
					 }
					 else {
						 throw new OperationNotAllowedException();
					 }
				}
}
